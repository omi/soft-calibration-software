# Soft Calibration Software


This tool takes the OMTO3 data and applies the screens based on the "Steve Taylor technique" 

The code screens based on...
```
Lon < 112 (Excludes dark sands of Australia) 
SZA < 50 (Excludes shadows from mountains)
Terrain Height < 1.2km (Exclude mountains)
R354-R331 < 0.01
0 < R331 < 0.08
snow_ice flag = 0 (excludes snow/ice and water) 
```
Then stores off all the screened data into an hdf5 file, creates a few plots, and calculated reflectvity envelops based on lowest 10% of screened data for each row (envelops are stored in a text file)

A few notes...

The variable max_refl defines the upper bound of 331nm reflectivity used for screening

The time range is defined by the variables...

year_st = year start
year_end = year_end
month_st = month start
month_end = month_end 

The first chunk of the code screens the data and saves to hdf5. If you don't wants plots made, just put a stop after the hdf5 file is created. 

There are 5 types of plots made...

1. Global map of scatter points showing the location of the screened data
2. 2 panel plot showing reflectivity vs deltaR and crosstrack vs deltaR
3. Plots showing reflectivity vs deltaR and deltaR & reflectivity histograms (created separately for each row)
4. Plots showing PDF of deltaR from each row overplotted and color coded based on part of the cross track (red-nadir, black-left rows, blue-right rows)
5. Calculated reflectivity envlopes as a function of crosstrack 
