import cartopy.crs as ccrs
from pathlib import Path
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import cartopy.feature as cfeature
import datetime
import h5py
import numpy as np
from pylab import *
import glob
import sys
import matplotlib
matplotlib.use('agg')
import time
import re
import matplotlib.pyplot as plt

plt.rc('font', size=20)          # controls default text sizes
plt.rc('axes', titlesize=30)     # fontsize of the axes title
plt.rc('axes', labelsize=25)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=20)    # fontsize of the tick labels
plt.rc('ytick', labelsize=20)    # fontsize of the tick labels
plt.rc('legend', fontsize=20)    # legend fontsize
plt.rc('figure', titlesize=25)  # fontsize of the figure title

def grab_files(year,month,day):
    files = glob.glob('/tis/OMI/95000/OMTO3/'+str(year)+'/'+str(mon).zfill(2)+'/'+str(day).zfill(2)+'/'+'*nc')
    return files

def make_map():
    fig, axes = plt.subplots(figsize=(11,8),subplot_kw=dict(projection=ccrs.PlateCarree()))
    axes.set_xticks(np.arange(-180,200,30))
    axes.set_yticks(np.arange(-90,110,20))
    axes.tick_params(axis='both', which='major', pad=7)
    axes.coastlines(resolution='50m',zorder=4)
    lakes = cfeature.NaturalEarthFeature(category='physical',name='lakes',scale='10m',facecolor='None',lw=0.3)
    axes.add_feature(lakes,edgecolor='k')
    return fig, axes

# Creating initial plots for looking at calibration data for July 2016. Data is plotted with the following screens:
# Lon < 112 (Excludes dark sands of Australia)
# SZA < 50 (Excludes shadows from mountains)
# Theight < 1.2km (Exclude Mountains)
### R354-R331 < 0.01
### 0. < R331 < 0.08
#Using ancillary snow/ice flag to avoid snow/ice or ocean (snow_ice = 0 is snow-free land)

# R331 is plotted as a function of Delta R (R354-R331) for the screened data
# R331 is plotted for all cross track postions for the screened data
max_refl = 0.08

year_st = 2005
year_end = 2005

keys = ['r331','r354','stp3_ozone','stp2_ozone','stp1_ozone','row','year','lat','lon','sza','vza','dndr_331','dndr_354']
var = {}
#Making dictionary of empty arrays for fields we are storing 
for key in keys:
    var[key] = np.array([])

month_st = 6
month_end = 8

#converting start and end month to string for plot title/filename
month_st_str = datetime.datetime(year_st,month_st,1).strftime('%b')
month_end_str = datetime.datetime(year_st,month_end,1).strftime('%b')

#Starting a plot with a map to make an image showing all screened data 
fig, axes = make_map()

#Wavelength indices for dndr
wave_ind_331 = 7
wave_ind_354 = 9

#looping through months/days
#Note that the upper bound of "range" in inclusive hence the +1. So range(2005,2006) is simply just 2005
for year in range(year_st,year_end+1):
    for mon in range(month_st,month_end+1):
        for day in range(1,32):
            files = grab_files(year,mon,day)
            if len(files) == 0:
                continue
            print('Month: ',str(mon)+' Day: '+str(day))
            print(files)

            for filename in files:
                print(filename)
                orb = re.findall('-o[0-9]{5}',filename)[0]

                #Reading in OMTO3 file. Reading in a few extra fields that can be used for analysis plots if desired such as ozone
                data = h5py.File(filename)
                r331 = data['/SCIDATA/Reflectivity340'][:]
                r354 = data['/SCIDATA/Reflectivity380'][:]
                ozone = data['/SCIDATA/ColumnAmountO3'][:]
                stp2_ozone = data['/SCIDATA/StepTwoO3'][:]
                stp1_ozone = data['/SCIDATA/StepOneO3'][:]
                r331[r331 == -2**100] = np.nan
                r354[r354 == -2**100] = np.nan
                lon = data['/GEODATA/longitude'][:]
                lat = data['/GEODATA/latitude'][:]
                sza = data['/GEODATA/solar_zenith_angle'][:]
                vza = data['/GEODATA/viewing_zenith_angle'][:]
                dndr = data['/SCIDATA/dNdR'][:]
                tpress = data['/SCIDATA/TerrainPressure'][:]
                theight= data['/GEODATA/surface_altitude'][:]/1000.
                gpqf = data['/GEODATA/ground_pixel_quality'][:]
                row = np.repeat(
                    np.arange(60)[np.newaxis, :], len(lon[:, 0]), axis=0)


                #Reading OMI ancillary file to get snow/ice and land/water info 
                anc_file = glob.glob('/tis/OMI/10004/OMUANC/'+str(year)+'/'+str(mon).zfill(2)+'/'+str(day).zfill(2)+'/*'+orb.replace('-o','-o0')+'*nc')
                if len(anc_file) == 0:
                    print('NO ANC')
                    continue
                d = h5py.File(anc_file[0],'r')
                snow_ice = d['/BAND2_ANCILLARY/STANDARD_MODE/ANCDATA/snow_ice'][0]
                
                #Screening based on criteria noted in comments at top of code 
                inds = (theight.flatten() < 1.2) &  (sza.flatten() < 50) &  (r331.flatten() > 0.)  & (r331.flatten() < 0.08) &(lon.flatten(
                ) < 112) & ((r354.flatten()-r331.flatten()) < 0.01) & (snow_ice.flatten() == 0) & (ozone.flatten() != -2**100) 
                
                #Adding the screened data points onto the global map 
                axes.scatter(lon.flatten()[inds],lat.flatten()[inds],color='k',lw=0,s=0.1)
                
                
                #Concatenating the current orbit to the list of data points for analysis 

                len_screen_data = len(r331.flatten()[inds])
                
                var['year'] = np.hstack(
                    (var['year'], [year]*len_screen_data))
                var['r331'] = np.hstack(
                    (var['r331'], r331.flatten()[inds]))                
                var['stp3_ozone'] = np.hstack(
                    (var['stp3_ozone'], ozone.flatten()[inds]))
                var['stp1_ozone'] = np.hstack(
                    (var['stp1_ozone'], stp1_ozone.flatten()[inds]))
                var['stp2_ozone'] = np.hstack(
                    (var['stp2_ozone'], stp2_ozone.flatten()[inds]))
                var['r354'] = np.hstack(
                    (var['r354'], r354.flatten()[inds]))
                var['row'] = np.hstack((var['row'], row.flatten()[inds]))
                var['lat'] = np.hstack((var['lat'], lat.flatten()[inds]))
                var['lon'] = np.hstack((var['lon'], lon.flatten()[inds]))
                var['sza'] = np.hstack((var['sza'], sza.flatten()[inds]))
                var['vza'] = np.hstack((var['vza'], vza.flatten()[inds]))
                var['dndr_331'] = np.hstack((var['dndr_331'], dndr[:,:,wave_ind_331].flatten()[inds]))
                var['dndr_354'] = np.hstack((var['dndr_354'], dndr[:,:,wave_ind_354].flatten()[inds]))
            data.close()


#Creating hdf5 file to store screened data for future analysis 
f = h5py.File('OMI_SoftCal_Data_'+month_st_str+'_'+month_end_str+'_'+str(year_st)+'_'+str(year_end)+'.h5','w')

#Adding metadata with the screening criteria used 
f.attrs['DataScreening'] = 'Lon<112, 0<R331<0.08, Terrain Height<1.2km,SZA<50,R354-R331<0.01'
for key in var.keys():
    f.create_dataset(key,data=var[key],dtype=np.float32, compression='gzip', compression_opts=9)
f.close()

#Saving map showing the screened data
plt.subplots_adjust(top=0.8)
plt.savefig('MaxRefl'+str(max_refl)+'_FirstPass.png')
plt.clf()
plt.close()

#Making a scatter plot showing deltaR as a function of reflectivity and as a fucntion of row for quick analysis 
fig, axes = plt.subplots(1, 2, figsize=(18, 5))
plt.subplots_adjust(left=0.1, bottom=0.15, right=0.95,
                    top=0.95, wspace=0.25, hspace=0.1)

axes[0].scatter(var['r331'], var['r354']-var['r331'], s=0.5, lw=0, color='k',alpha=0.2)
axes[1].scatter(var['row'], var['r354']-var['r331'], s=2, lw=0, color='k',alpha=0.2)

axes[0].set_xlim(0.0, 0.08)
axes[0].set_ylim(-0.04, 0.01)

axes[1].set_xlim(0, 60)
axes[1].set_ylim(-0.04, 0.01)

axes[0].set_ylabel('354-331nm Refl', fontsize=25)
axes[0].set_xlabel('R331', fontsize=25)
axes[1].set_xlabel('Row', fontsize=25)

axes[1].set_ylabel('354nm-331nm Refl', fontsize=25)

plt.savefig(month_st_str+'_'+month_end_str+'_MinRefl_OMI_UpdV9_MaxRefl'+str(max_refl)+'_FirstPass.png')
plt.clf()
plt.close()



#Scatter plots and PDFs of the data are plotted from the above screened data for each row to look at the change from row to row.
for row in range(60):
    fig, axes = plt.subplots(2, 1, figsize=(10, 14))
    plt.subplots_adjust(left=0.1, bottom=0.2, right=0.95,
                        top=0.9, wspace=0.25, hspace=0.2)

    #Making a scatter plot of deltaR vs reflectivity, similar to first plot but doing this for each row
    axes[0].scatter(var['r331'][var['row'] == row], var['r354'][var['row']
                                                                == row]-var['r331'][var['row'] == row], s=2, lw=0, color='k',alpha=0.2)
    axes[0].plot([-0.01, 0.15], [0.0, 0.0], lw=2, color='k')

    #Making a PDF of deltaR and reflectivity
    axes[1].hist(var['r331'][var['row'] == row], bins=np.arange(-0.01,
                                                                max_refl+0.005, 0.005), lw=2, color='k', histtype='step',label='R331')
    axes[1].hist(var['r331'][var['row'] == row]-var['r354'][var['row'] == row],
                 bins=np.arange(-0.01, 0.015, 0.005), lw=2, color='b', histtype='step',label='DeltaR')

    axes[1].legend()
    axes[0].set_xlim(-0.01, max_refl)
    axes[0].set_ylim(-0.02, 0.02)

    axes[1].set_xlim(-0.01, max_refl)
    axes[1].set_ylim(0, 5000)

    axes[0].set_ylabel('354-331nm Refl', fontsize=25)
    axes[0].set_xlabel('331nm Refl', fontsize=25)

    axes[1].set_ylabel('Npt', fontsize=25)
    axes[1].set_xlabel('331nm Refl/DeltaR', fontsize=25)

    plt.suptitle(month_st_str+'-'+month_end_str+' '+str(year_st)+'-'+str(year_end)+' Row '+str(row).zfill(2), fontsize=40)
    Path('V9_MinRefl_Rows').mkdir(parents=True, exist_ok=True)
    plt.savefig('V9_MinRefl_Rows/Seasonal_MinRefl_OMI_Row' +
                str(row).zfill(2)+'_MaxRefl'+str(max_refl)+'_FirstPass.png', bbox_inches='tight')
    plt.clf()
    plt.close()

# Creating a histogram of R331 for each row to help in determining the "Statistical Base Minimum Value", basically determine a "lowest" percentile of the data which there is enough data for analysis.
# This part of the analysis is a bit "handy wavy" and probably the most questionable part of the analysis as there is no good reason to choose one percentile over another. For this analysis the 10% percentile
# was used by Steve for analysis. (First section simply colors the nadir rows as well as row 25 which is an outlier)
fig, axes = plt.subplots(1, 1, figsize=(14, 10))
for row in range(60):
        
    plt.subplots_adjust(left=0.1, bottom=0.2, right=0.95,
                        top=0.9, wspace=0.25, hspace=0.1)
        
    temp_data = np.array(
        var['r331'][(var['r331'] < max_refl) & (var['row'] == row)])
    temp_data = np.array(temp_data[~np.isnan(temp_data)])
    weights = np.ones_like(temp_data)/float(len(temp_data))

    #Plotting nadir rows as red, left rows as black, and right rows as blue
    if (row > 25) & (row < 35):
        axes.hist(temp_data, bins=np.arange(-0.01, max_refl+0.0001, 0.0001), histtype='step',
                  lw=2, density=0, weights=weights, color='red', cumulative=True)
    elif row < 26:
        axes.hist(temp_data, bins=np.arange(-0.01, max_refl+0.0001, 0.0001),
                  histtype='step', density=0, weights=weights, color='k', cumulative=True)
    elif row > 35:
        axes.hist(temp_data, bins=np.arange(-0.01, max_refl+0.0001, 0.0001),
                  histtype='step', density=0, weights=weights, color='blue', cumulative=True)

    axes.plot([-0.01, max_refl], [0., 0.], '--k')
    axes.set_xlim(-0.01, max_refl)
    axes.set_ylim(0, 1)

    axes.set_ylabel('Fraction', fontsize=25)
    axes.set_xlabel('331nm Refl', fontsize=25)

    axes.text(-0.005, 0.8, 'Nadir Rows (26-35) - Red', fontsize=25)
    axes.text(-0.005, 0.6, 'Left Rows (1-25) - Black', fontsize=25)
    axes.text(-0.005, 0.7, 'Right Rows (36-60) - Blue', fontsize=25)

    plt.suptitle(month_st_str+'-'+month_end_str+' '+str(year_st)+'-'+str(year_end), fontsize=40)
plt.subplots_adjust(top=0.85)
plt.savefig('MinRefl_Dist_UpdV9_MaxRefl'+str(max_refl)+'_FirstPass.png', bbox_inches='tight')
plt.clf()
plt.close()


# Based on the lower 10% percentile, simply comparing the envelop values Zach calculated with those Steve calculated. Very similar except for rows 31-34, possibly due to one method
# having "inclusive" percentile cutoff while other method is "exclusive" but reason still unknown.

# Calculating the env_331elop values based on lowest 10 percentile of data for the screened data.
env_331 = []
env_354 = []
for row in range(60):
    temp_331 = np.array(
        var['r331'][(var['r331'] < max_refl) & (var['row'] == row)])
    temp_354 = np.array(
        var['r354'][(var['r354'] < max_refl) & (var['row'] == row)])

    if (len(temp_331) == 0) | (len(temp_354) == 0):
        env_331.append(np.nan)
        env_354.append(np.nan)
        continue
    temp_331 = sorted(np.array(temp_331[~np.isnan(temp_331)]))
    temp_354 = sorted(np.array(temp_354[~np.isnan(temp_354)]))

    env_331.append(np.nanmean(temp_331[int(len(temp_331)*.1)]))
    env_354.append(np.nanmean(temp_354[int(len(temp_354)*.1)]))


#Storing envelop values into simple text file
np.savetxt('Envelopes_'+str(max_refl)+'_MaxRefl'+str(max_refl)+'_FirstPass.txt',np.transpose([np.arange(60),env_331,env_354]),header='Row, 331_Env, 354_Env',comments='')

#Plotting calculated evenlop values as a function of crosstrack position
fig, axes = plt.subplots(2, 1, figsize=(15, 7))
axes[0].plot(np.arange(60), env_331, '-o', lw=2, color='r',label='331nm')
axes[0].plot(np.arange(60), env_354, '-o', lw=2, color='b',label='354nm')

axes[1].plot(np.arange(60), np.array(env_354)-np.array(env_331), '-o', lw=2, color='k',label='354nm')

axes[0].legend(fontsize=15,loc='upper center',ncol=2)
axes[0].set_xlim(0, 60)
axes[1].set_xlim(0, 60)
axes[0].set_ylim(-0.01, max_refl)
axes[1].set_xlabel('Crosstrack #', fontsize=30)
axes[0].set_ylabel('Refl Env', fontsize=30)
axes[1].set_ylabel('R354-R331 Env', fontsize=30)
plt.suptitle(month_st_str+'-'+month_end_str+' '+str(year_st)+'-'+str(year_end)+' OMI', fontsize=40)
plt.subplots_adjust(top=0.85)

plt.savefig('OMI_Cal_UpdV9_MaxRefl'+str(max_refl)+'_FirstPass.png', bbox_inches='tight')
plt.clf()
plt.close()

